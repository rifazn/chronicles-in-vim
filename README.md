# Chronicles in Vim

I am often in a situation when I need to store an information about someone/something. How I do that is that I use plain simple vim along with the person's name to store that information. This is a small set of scripts to help me do that more easily (by using XDG desktop files).

## Who is this for?

Its for those who like to use vim and like taking notes with the freedom and simplicity of a text editor.

## Requirements

`dialog` and either of `vim` or `nvim` is required to run this program.

## How to

To add a new chronicle entry: Search for 'chronicles' in your app launcher and select 'new Chronicle'.

To modify an existing chronicle: Search for 'Chronicles' in your app launcher and select the name from the menu provided there.

## How this works

This just creates XDG desktop files that opens a file (the chronicle) in vim.
