#!/bin/bash

value_file=/tmp/chronicles_$$.txt
program_dir=$HOME/try/chronicles/chronicles-run.sh
data_dir=$HOME/try/chronicles/data
desktop_dir="$HOME/.local/share/applications/chronicles"
mkdir -p $data_dir
mkdir -p $desktop_dir

mkdesktop() {
    # convert all characters from upper case to lower case
    # this is a requirement for making desktop files
    name=$(echo "$1" | tr [:upper:] [:lower:])
    # Check size of name here first. Skipping for now

    cat > $desktop_dir/"$name".desktop <<!FUNKY!
[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=$name
Comment=Take notes for $name
Exec=$program_dir
Terminal=true
Icon=terminal
!FUNKY!
    chmod +x $desktop_dir/"$name".desktop
}

if [ ! -f $desktop_dir/chronicles.desktop ]
then
    mkdesktop "Chronicles"
fi

entries=($(ls $data_dir))
i=0
j=1
for entry in ${entries[@]}
do
    options[$i]=$j
    i=$(($i + 1))
    options[$i]="$entry"
    i=$(($i + 1))
    options[$i]="off"
    i=$(($i + 1))
    j=$(($j + 1))
done

echo ${options[*]}

dtext="Which chronicle would you like to edit?"
dh=15
dw=50
dnums=5
dialog --title Chronicles --radiolist "$dtext" "$dh" "$dw" "$dnums" \
    "${options[@]}" \
    "A" "Or, add a new chronicle entry." on \
    "E" "Edit the source code of this program." off \
    2> $value_file

dval=$(cat $value_file)
rm $value_file
clear
echo $dval

case $dval in
    "A" )   # add a new entry here
            echo "Add a new entry"
            dialog --inputbox "What shall be the name of the chronicle?" \
                7 40 2> $value_file
            name=$(cat $value_file)
            nvim "$data_dir/$name"
            ;;
    "E" )   nvim $program_dir
            ;;
    "" )    exit 2
            ;;
    * )     nvim $data_dir/${entries[$(($dval - 1))]}
            ;;
esac

exit 0
